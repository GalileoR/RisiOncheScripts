// ==UserScript==
// @name         ONCHErche
// @version      0.1.0
// @description  Recherche mieux intégrée avec la liste des topics
// @author       RisiOnche
// @match        https://onche.org/forum/*
// @exclude      https://onche.org/forum/**/search
// @exclude      https://onche.org/forum/**/search?*
// @icon         https://www.risishack.com/0dnn2f.png
// @grant        none
// @downloadURL  https://codeberg.org/GalileoR/RisiOncheScripts/raw/branch/main/oncherche.user.js
// @updateURL    https://codeberg.org/GalileoR/RisiOncheScripts/raw/branch/main/oncherche.user.js
// ==/UserScript==

(function() {
    'use strict';
    const url = window.location.href;
    const searchHTML = `
<div class=""><div id="forum" data-id="1">
    <form id="oncherche" class="content row" method="GET" action="${url}/search">
      <input class="input margin-right" type="text" name="q" maxlength="25" placeholder="Rechercher quelque chose" autocomplete="off" autofocus="">
      <button class="button filled medium">
        <div class="mdi mdi-magnify"></div>
      </button>
    </form>
</div></p>
    `;
    const divTitle = document.querySelector('.title.is_sticky');
    if(divTitle) divTitle.insertAdjacentHTML('afterend', searchHTML);
})();
