// ==UserScript==
// @name         RisiOnche
// @version      0.8.3
// @description  Risibank sur onche.org
// @author       RisiOnche
// @match        https://onche.org/topic/*
// @match        https://onche.org/forum/*
// @match        https://onche.org/chat/*
// @exclude      https://onche.org/forum/**/search
// @exclude      https://onche.org/forum/**/search?*
// @icon         https://www.risishack.com/lfoubw.png
// @grant        GM_setValue
// @grant        GM_getValue
// @require      https://risibank.fr/downloads/web-api/risibank.js
// @downloadURL  https://codeberg.org/GalileoR/RisiOncheScripts/raw/branch/main/risionche.user.js
// @updateURL    https://codeberg.org/GalileoR/RisiOncheScripts/raw/branch/main/risionche.user.js
// ==/UserScript==

const storageKey = 'risi-integ';
let integ = GM_getValue(storageKey, true);
const sizing = '10rem';
const bodyClassName = document.querySelector("body").className;
let theme = bodyClassName.includes("blue") ? 'dark' : (bodyClassName.includes("grey") ? 'dark' : 'light');

function activate(theme, container){
    RisiBank.activate({
        type: integ ? 'iframe' : 'overlay',
        container: container ,
        openPosition: undefined,
        theme: theme,
        mediaSize: 'sm',
        navbarSize: 'sm',
        defaultTab: 'fav',
        showNSFW: true,
        allowUsernameSelection: true,
        showCopyButton: false,
        onCopyMedia: ({ id, type, media }) => { console.log(media, 'copy'); },
        onSelectMedia: RisiBank.addSourceImageToTextArea(document.querySelector('.textarea') || document.querySelector('.Form__input'))
    });
}
let refresh = false;
(function() {
    'use strict';
    let quoteButtons;
    setInterval(() => {
        if(!refresh && document.querySelector('.panel.panel-jv-forum')){
            refresh = true;
            document.querySelector("iframe")?.remove();
            //theme = 'dark';
            if(integ) activate(theme, container);
            const iframe = document.querySelector("iframe");
            if(iframe.src.startsWith("https://risibank.fr/embed")) iframe.style.height = '8rem';
        }

        Array.from(document.querySelectorAll('.item'))?.filter(el => el.textContent === 'Répondre')?.forEach(quoteButton => quoteButton.onclick = function(){
            let newContainer = document.querySelector(".favoriteStickers.margin-bottom");
            let alreadyExistingRisiOnche = document.querySelector('.quote-risionche');
            newContainer.insertAdjacentHTML('afterend', '<div class="quote-risionche"></div>');
            const quote = document.querySelector('.quote-risionche');
            if(newContainer) newContainer.style.maxHeight = '30rem';
            if(!alreadyExistingRisiOnche) {
                if(integ){
                    activate(theme, document.querySelector('.quote-risionche'));
                    const iframe = document.querySelector("iframe");
                    if(iframe.src.startsWith("https://risibank.fr/embed")) iframe.style.height = sizing;
                }else{
                    let newLogo = document.createElement('img');
                    newLogo.id = 'risi-trigger';
                    newLogo.src = "http://i.imgur.com/V9OsBLW.png";
                    newLogo.className = "content centered"
                    quote.append(newLogo);
                    document.querySelector('#risi-trigger').onclick = function(){
                        activate(theme, quote);
                    };
                }
            }
        });
        document.querySelectorAll('.mdi.mdi-share')?.forEach(quoteButton => quoteButton.onclick = function(){
            let newContainer = document.querySelector(".favoriteStickers.margin-bottom");
            let alreadyExistingRisiOnche = document.querySelector('.quote-risionche');
            newContainer.insertAdjacentHTML('afterend', '<div class="quote-risionche"></div>');
            const quote = document.querySelector('.quote-risionche');
            if(newContainer) newContainer.style.maxHeight = '30rem';
            if(!alreadyExistingRisiOnche) {
                if(integ){
                    activate(theme, document.querySelector('.quote-risionche'));
                    const iframe = document.querySelector("iframe");
                    if(iframe.src.startsWith("https://risibank.fr/embed")) iframe.style.height = sizing;
                }else{
                    let newLogo = document.createElement('img');
                    newLogo.id = 'risi-trigger';
                    newLogo.src = "http://i.imgur.com/V9OsBLW.png";
                    newLogo.className = "content centered"
                    quote.append(newLogo);
                    document.querySelector('#risi-trigger').onclick = function(){
                        activate(theme, quote);
                    };
                }
            }
        });
        document.querySelectorAll('.material-icons.onchat-quoting')?.forEach(quoteButton => quoteButton.onclick = function(){
            let newContainer = document.querySelector(".favoriteStickers.margin-bottom");
            let alreadyExistingRisiOnche = document.querySelector('.quote-risionche');
            newContainer.insertAdjacentHTML('afterend', '<div class="quote-risionche"></div>');
            const quote = document.querySelector('.quote-risionche');
            if(newContainer) newContainer.style.maxHeight = '30rem';
            if(!alreadyExistingRisiOnche) {
                if(integ){
                    activate(theme, document.querySelector('.quote-risionche'));
                    const iframe = document.querySelector("iframe");
                    if(iframe.src.startsWith("https://risibank.fr/embed")) iframe.style.height = sizing;
                }else{
                    let newLogo = document.createElement('img');
                    newLogo.id = 'risi-trigger';
                    newLogo.src = "http://i.imgur.com/V9OsBLW.png";
                    newLogo.className = "content centered"
                    quote.append(newLogo);
                    document.querySelector('#risi-trigger').onclick = function(){
                        activate(theme, quote);
                    };
                }
            }
        });
        document.querySelectorAll('.material-icons.ontchat-picto.ontchat-quote.picto-msg-quote')?.forEach(quoteButton => quoteButton.onclick = function(){
            let newContainer = document.querySelector(".favoriteStickers.margin-bottom");
            let alreadyExistingRisiOnche = document.querySelector('.quote-risionche');
            newContainer.insertAdjacentHTML('afterend', '<div class="quote-risionche"></div>');
            const quote = document.querySelector('.quote-risionche');
            if(newContainer) newContainer.style.maxHeight = '7rem';
            if(!alreadyExistingRisiOnche) {
                if(integ){
                    activate(theme, document.querySelector('.quote-risionche'));
                    const iframe = document.querySelector("iframe");
                    if(iframe.src.startsWith("https://risibank.fr/embed")) iframe.style.height = '7rem';
                }else{
                    let newLogo = document.createElement('img');
                    newLogo.id = 'risi-trigger';
                    newLogo.src = "http://i.imgur.com/V9OsBLW.png";
                    newLogo.className = "content centered"
                    quote.append(newLogo);
                    document.querySelector('#risi-trigger').onclick = function(){
                        activate(theme, quote);
                    };
                }
            }
        });
    }, 400);
    document.querySelectorAll("a.link[target='_blank']")?.forEach(link => {
        const url = link.href;
        if(url.startsWith("https://risibank.fr/cache/medias/")){
            let newImage = document.createElement('img');
            let div = document.createElement('div');
            div.className = 'smiley';
            newImage.src = url;
            div.append(newImage);
            link.replaceWith(div);
        }
    });
    const newBlock = `
<div class="bloc border red">
  <div class="title">🔧 Risibank</div>
  <div class="content centered ">
    <button id="integ" class="button bordered" type="button">Mode intégré ON/OFF</button>
  </div>
</div>
    `;
    const purples = document.querySelectorAll('.bloc.border.purple');
    const adBloc = document.querySelectorAll('.bloc.border.purple')[purples.length-1];
    adBloc?.insertAdjacentHTML('afterend', newBlock);
    let favoriteStickers = document.querySelector(".favoriteStickers.margin-bottom");
    if(favoriteStickers) favoriteStickers.insertAdjacentHTML('afterend', '<div class="quote-risionche"></div>');
    let container = document.querySelector(".quote-risionche") || document.querySelector("#answer") || document.querySelector(".screen");
    if(container) container.style.maxHeight = '30rem';
    document.querySelector('#integ').onclick = function(){
        integ = !integ;
        GM_setValue(storageKey, integ);
        const alert = document.querySelector('.alert.green.margin-bottom.vanish');
        if(!alert) document.querySelector('.bloc.border.red')?.insertAdjacentHTML('afterend', '<div class="alert green margin-bottom vanish">Sauvergardé. Sera pris en compte après avoir actualisé la page</div>');
    };
    if(integ) {
        activate(theme, container);
        const iframe = document.querySelector("iframe");
        if(iframe.src.startsWith("https://risibank.fr/embed")) iframe.style.height = sizing;
    } else {
        let newLogo = document.createElement('img');
        newLogo.id = 'risi-trigger';
        newLogo.src = "http://i.imgur.com/V9OsBLW.png";
        newLogo.className = "content centered"
        container.append(newLogo);
        document.querySelector('#risi-trigger').onclick = function(){
            activate(theme, container);
        };
    }
})();