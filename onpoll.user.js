// ==UserScript==
// @name         OnPoll
// @version      0.1.2
// @description  Sondage strawpoll dans vos topics Onche !
// @author       RisiOnche
// @match        https://onche.org/topic/*
// @icon         https://www.risishack.com/vljrgv.png
// @grant        none
// @downloadURL  https://codeberg.org/GalileoR/RisiOncheScripts/raw/branch/main/onpoll.user.js
// @updateURL    https://codeberg.org/GalileoR/RisiOncheScripts/raw/branch/main/onpoll.user.js
// ==/UserScript==

(function() {
    'use strict';

    document.querySelectorAll("a.link[target='_blank']")?.forEach(link => {
        const url = link.href;
        if(url.startsWith("https://strawpoll.com/polls/")){
            const id = url.replace('https://strawpoll.com/polls/', '').replace('/results', '');
            link.insertAdjacentHTML('afterEnd', `<div class="strawpoll-embed" id="strawpoll_${id}" style="height: 480px; max-width: 640px; width: 100%; display: flex; flex-direction: column;"><iframe title="StrawPoll Embed" id="strawpoll_iframe_${id}" src="https://strawpoll.com/embed/polls/${id}/results" style="position: static; visibility: visible; display: block; width: 100%; flex-grow: 1;" frameborder="0" allowfullscreen allowtransparency>Loading...</iframe><script async src="https://cdn.strawpoll.com/dist/widgets.js" charset="utf-8"></script></div>`);
        }
    });
})();
