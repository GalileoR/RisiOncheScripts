// ==UserScript==
// @name         ONCHistory
// @version      0.4.6
// @description  try to take over the world!
// @author       RisiOnche
// @match        https://onche.org/forum/*
// @match        https://onche.org/topic/*
// @match        https://onche.org
// @icon         https://www.risishack.com/lkbb9w.png
// @updateURL    https://codeberg.org/GalileoR/RisiOncheScripts/raw/branch/main/onchistory.user.js
// @downloadURL  https://codeberg.org/GalileoR/RisiOncheScripts/raw/branch/main/onchistory.user.js
// @grant        GM_setValue
// @grant        GM_getValue
// ==/UserScript==

(async function() {
    'use strict';

    const newBlockTopics = `
<div class="bloc border green">
  <div class="title">🕓 Mes 10 derniers topics visités</div>
  <div class="topics-script content links"></div>
</div>
    `;
    const newBlockPosts = `
<div class="bloc border orange">
  <div class="title">🕓 Mes 8 derniers posts</div>
  <div class="posts-script content links"></div>
</div>
    `;
    const newBlockFavs = `
<div class="bloc border red">
  <div class="title">🌟 Mes favoris</div>
  <div class="announcement"><div class="announcement-content rmjs-1">Ne se met à jour que lorsque que vous allez sur <a href="https://onche.org/forum/favorites">la liste des favoris</a> à cause d'un bug du fofo</div></div>
  <div class="fav-script content links"></div>
  <div class="content links"><a href="https://onche.org/forum/favorites">Voir plus</a></div>
</div>
    `;
    const cosBlock = document.querySelector('.bloc.border.green');
    cosBlock.insertAdjacentHTML('afterend', newBlockPosts);
    cosBlock.insertAdjacentHTML('afterend', newBlockTopics);
    cosBlock.insertAdjacentHTML('afterend', newBlockFavs);
    const favs = document.querySelector('.fav-script.content.links');
    const topics = document.querySelector('.topics-script.content.links');
    const posts = document.querySelector('.posts-script.content.links');
    const storageKey = 'onchistorysgg';
    const storageKeyTitles = 'onchistory-titless';
    const storageKeyFavs = 'onchistory-favs';
    let url = window.location.href;
    let currentHistory = GM_getValue(storageKey, '')?.split(';')?.filter(Boolean);
    let titles = GM_getValue(storageKeyTitles, '')?.split('!#!')?.filter(Boolean);
    if(url.startsWith('https://onche.org/topic/')){
        url = url.match(/https:\/\/onche\.org\/topic\/\d+\/([a-zA-Z\d]*(-)*)*/)[0];
        if(!currentHistory.includes(url)) {
            let title = document.querySelector('h1')?.innerHTML;
            titles.push(title);
            currentHistory.push(url);
        }
        if(currentHistory.length>10){
            currentHistory = currentHistory.slice(1,10);
            titles = titles.slice(1,10);
        }
        GM_setValue(storageKey, currentHistory.join(';'));
        GM_setValue(storageKeyTitles, titles.join('!#!'));
    }
    let i = 0;
    currentHistory.forEach((entry) => {
       if(titles[i]) topics.insertAdjacentHTML('beforeend', `<a href="${entry}">${titles[i]}</a>`);
        i++;
    });
    try{
        const res = await fetch('https://onche.org/', {headers: {'Cache-Control': 'no-cache'}});
        const text = await res?.text();
        const parser = new DOMParser();
        const dom = parser.parseFromString(text, 'text/html');
        const blocs = dom.querySelectorAll('.bloc');
        blocs?.forEach(bloc => {
            let title = bloc.querySelector('.title')?.innerHTML;
            if(title !== '<div class="mdi mdi-comment margin-right"></div>Mes derniers messages') return;
            let ps = bloc.querySelectorAll('.topic-subject.link');
            ps.forEach(post => {
                let text = post.querySelector('span')?.innerHTML;
                if(text) posts.insertAdjacentHTML('beforeend', `<a href="${post.href}">${text}</a>`);
            });
        });
    }catch(e){
        console.log(`Tried to fetch latest posts and failed. Exception : ${e}`);
    }
    try{
        const favorites = [];
        if(url.startsWith('https://onche.org/forum/favorites')){
            /*const res = await fetch('https://onche.org/forum/favorites');
            const text = await res?.text();
            const parser = new DOMParser();
            const dom = parser.parseFromString(text, 'text/html');*/
            const topics = document.querySelectorAll('.topic-subject.link');
            topics?.forEach(topic => {
                let text = topic.querySelector('span')?.innerHTML;
                if(text) favorites.push(`<a href="${topic.href}">${text}</a>`);
                i++;
            });
            GM_setValue(storageKeyFavs, favorites.join('!#!'));
        }
        i = 0;
        GM_getValue(storageKeyFavs, '')?.split('!#!')?.filter(Boolean)?.forEach(topic => {
            if(i>=10) return;
            favs.insertAdjacentHTML('beforeend', topic);
            i++;
        });
    }catch(e){
        console.log(`Tried to fetch latest favs and failed. Exception : ${e}`);
    }
})();