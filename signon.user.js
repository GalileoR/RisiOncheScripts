// ==UserScript==
// @name         SignON
// @version      0.2.0
// @description  Affiche ta signature sur Onche.org
// @author       RisiOnche
// @match        https://onche.org/topic/*
// @match        https://onche.org/account/profil
// @icon         https://www.risishack.com/po8c1f.png
// @updateURL    https://codeberg.org/GalileoR/RisiOncheScripts/raw/branch/main/signon.user.js
// @downloadURL  https://codeberg.org/GalileoR/RisiOncheScripts/raw/branch/main/signon.user.js
// @grant        GM_setValue
// @grant        GM_getValue
// ==/UserScript==

function escapeRegex(string) {
    return string.replace(/[/\-\\^$*+?.()|[\]{}]/g, '\\$&');
}


(function () {
    'use strict';
    const pseudo = document.querySelector('.account > span')?.innerHTML;
    const storageKey = `sign-${pseudo}`;
    const storageKeyShowSign = "showSign";
    let signature = GM_getValue(storageKey) || '';
    let showSign = GM_getValue(storageKeyShowSign, true);

    const url = window.location.href;
    //if(url.includes('#last')) url = `${url}#last`;

    const form = document.querySelector(`form[action="${url}]`);
    document.querySelector('.button.medium.filled.right')?.addEventListener('click', () => {
        if (signature) document.querySelector('textarea[name="message"]').value += "\n\n[i]~[u]Signature[/u][/i]~" + signature;
    });

    if (url === "https://onche.org/account/profil") {
        const newBlock = `
    <div class="bloc"><div class="title">[SCRIPT] Gestion de la signature</div>
    <div class="content"><div class="alert blue margin-bottom">Toutes les options de formatage supportées par le forum (gras, souligné, italiques, ...) sont aussi disponibles pour la signature</div></div>
    <textarea id="text-sign" class="textarea centered" name="signature" maxlength="10000" placeholder="Créez votre signature">${signature}</textarea>
    <div class="content centered "><button id="sign" class="button bordered" type="button">Sauvegarder</button>
</div></div>
    `;
        document.querySelector('#left').insertAdjacentHTML('afterbegin', newBlock);
        document.querySelector('#sign').onclick = function () {
            GM_setValue(storageKey, document.querySelector('#text-sign')?.value || '');
            const alert = document.querySelector('.alert.green.margin-bottom.vanish');
            if (!alert) document.querySelector('#text-sign')?.insertAdjacentHTML('afterend', '<div class="alert green margin-bottom vanish">Sauvergardé</div>');
        };
    }
    const signBloc = `
<div class="bloc border grey">
    <div class="title">🔧 SignOn</div>
    <div class="content centered ">
    <button id="signB" class="button bordered" type="button">Signatures ON/OFF</button>
    </div>
</div>
    `;
    const adBloc = document.querySelector('.bloc.border.purple');
    adBloc.insertAdjacentHTML('afterend', signBloc);

    document.querySelector('#signB').onclick = function () {
        showSign = !showSign;
        GM_setValue(storageKeyShowSign, showSign);
        const alertSign = document.querySelector('#signAlert');
        if (!alertSign) adBloc.insertAdjacentHTML('afterend', '<div id="signAlert" class="alert green margin-bottom vanish">Sauvegardé. Sera pris en compte après avoir actualisé la page</div>');
    };
    const posts = document.querySelectorAll('.message-content');
    posts.forEach(post => {
        let text = post.innerHTML;
        const parts = text.split("<br><i>~<u>Signature</u></i>~");
        if (showSign && parts.length > 1) {
            let signDisplay = `
    <div class="message grey">
        <div class="" style="height: auto; max-height: 100px;">
        ${parts[parts.length - 1]}
        </div>
    </div>
    `;
            text = text.replace('<i>~<u>Signature</u></i>', signDisplay);
            let reg = new RegExp(escapeRegex((parts[parts.length - 1]).replace('', '')) + '$');
            text = text.replace('~', '').replace(reg, '');
            post.innerHTML = text;
        } else {
            text = text.replace('<i>~<u>Signature</u></i>', '');
            if (parts.length > 1) text = text.replace('~', '').replace(new RegExp(escapeRegex((parts[parts.length - 1]).replace('', '')) + '$'), '');
            post.innerHTML = text;
        }
    });

})();