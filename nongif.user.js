// ==UserScript==
// @name         nonGIF
// @license      MIT
// @version      0.1.6
// @description  Desactive tous les gifs
// @author       RisiOnche
// @match        https://onche.org/topic/*
// @icon         https://www.risishack.com/v63z2f.png
// @updateURL    https://codeberg.org/GalileoR/RisiOncheScripts/raw/branch/main/nongif.user.js
// @downloadURL  https://codeberg.org/GalileoR/RisiOncheScripts/raw/branch/main/nongif.user.js
// @grant        none
// @require      https://raw.githubusercontent.com/krasimir/gifffer/master/build/gifffer.min.js
// ==/UserScript==

function hasExt(url) {
    let parts = url?.split('/');
    if(!parts) return false;
    let last = parts.pop();
    return ( parts.length > 3 ) && ( last.indexOf('.') != -1 );
}

function griff(sticker){
    let image = sticker.querySelector('img');
    let src = image.src;
    if((hasExt(src) && src?.match(/\.(gif)$/) != null) || !hasExt(src)){
        image.removeAttribute('src');
        image.setAttribute('data-gifffer', src);
        image.setAttribute('data-gifffer-duration', 10000);
    }
}

function handleGifs(selectors){
    selectors?.forEach(selector => {
        document.querySelectorAll(selector)?.forEach(el => {
            griff(el);
        });
    });
}

(function() {
    'use strict';
    handleGifs(['.sticker:not(.favorite)', '.smiley', '._format._gif', '.flex']);
    Gifffer({
        playButtonStyles: {
            'width': '10px',
            'height': '10px',
            'border-radius': '10px',
            'background': 'rgba(0, 0, 0, 0.1)',
            'position': 'absolute',
            'top': '50%',
            'left': '50%',
            'margin': '-30px 0 0 -30px'
        },
        playButtonIconStyles: {
            'width': '0',
            'height': '0',
            'border-top': '1px solid transparent',
            'border-bottom': '14px solid transparent',
            'border-left': '14px solid rgba(0, 0, 0, 0.5)',
            'position': 'absolute',
            'left': '26px',
            'top': '16px'
        }
    });
})();